<?php


function bp960_form_system_theme_settings_alter(&$form, $form_state) {
	$form['googleanalytics'] = array(
    	'#type'          => 'fieldset',
    	'#title'         => t('Google Analytics settings'),
  	);
  	$form['googleanalytics']['bp960_google_analytics_status'] = array(
  		'#type'          => 'checkbox',
  		'#title'         => t('Activate Google Analytics.'),
  		'#default_value' => theme_get_setting('bp960_google_analytics_status'),
  		'#states' 		=> array(
	     	'checked' => array(
	        	':input[name="bp960_google_analytics_id"]' => array('filled' => TRUE),
	     	),
     	),
  	);
  	$form['googleanalytics']['bp960_google_analytics_id'] = array(
  		'#type'			=>	'textfield',
  		'#title'        => t('Site ID'),
  		'#description'	=> t('Enter your Google Analytics Site ID (UA-XXXXXXX-X).'),
  		'#states' 		=> array(
      		'invisible' 	=> array(
        		':input[name="bp960_google_analytics_status"]' => array('checked' => FALSE),
      		),
    	),
    	'#default_value'=> theme_get_setting('bp960_google_analytics_id')
  	);
  	
  	$form['htaccess'] = array(
    	'#type'          => 'fieldset',
    	'#title'         => t('.htaccess settings'),
  	);
  	$form['htaccess']['bp960_overwrite_htaccess'] = array(
  		'#type'          => 'radios',
  		'#options'		 => array('d7' => 'Original Drupal 7 .htaccess', 'bp' => 'Boilerplate Drupal 7 .htaccess File'),
  		'#title'         => t('Select what .htaccess File should be used:'),
  		'#default_value' => is_null(theme_get_setting('bp960_overwrite_htaccess'))?'d7':theme_get_setting('bp960_overwrite_htaccess'),
  		'#description'	 => t('Requires write access to .htaccess file owner and/or group.')
  	);
  	
  	$form['debug'] = array(
    	'#type'          => 'fieldset',
    	'#title'         => t('debug settings'),
  	);
  	$form['debug']['bp960_debug_grid'] = array(
  		'#type'          => 'checkbox',
  		'#title'         => t('Enable 960gs Grid Debugging.'),
  		'#default_value' => theme_get_setting('bp960_debug_grid'),
  	);
  	
  	$form['#bp960_htaccess_selected'] = theme_get_setting('bp960_overwrite_htaccess');
  	$form['#validate'][] = 'bp960_form_system_theme_settings_validate';
}

function bp960_form_system_theme_settings_validate($form_id, &$form_state){
	if($form_state['values']['bp960_google_analytics_status'] && empty($form_state['values']['bp960_google_analytics_id']))
	{
		form_set_error('bp960_google_analytics_id', t('You have to enter a Site ID to enable Google Analytics.'));
	}
	if(!$form_state['values']['bp960_google_analytics_status'] && !empty($form_state['values']['bp960_google_analytics_id']))
	{
		$form_state['values']['bp960_google_analytics_id'] = '';
	}
	// copy (backup) & replace .htaccess with the one from selection
	bp960_htaccess($form_state);
}


function bp960_htaccess($form_state){
	$htaccess = '.htaccess';
	$htaccess_d7 = 'd7.htaccess';
	$htaccess_bp = drupal_get_path('theme', 'bp960') . '/misc/bp_d7.htaccess';
	$htaccess_current = $form_state['complete form']['#bp960_htaccess_selected'];
	// set boilerplate .htaccess to current one
	if($form_state['values']['bp960_overwrite_htaccess'] == 'bp' && $htaccess_current != 'bp')
	{
		if(is_writable($htaccess))
		{
			if(copy($htaccess, $htaccess_d7))
			{
				if(copy($htaccess_bp, $htaccess))
				{
					drupal_set_message(t('Drupal7 .htaccess successfully replaced with HTML5 Boilerplate Drupal7 version.'));	
				}
				else{
					drupal_set_message(t('Error while copy HTML5 Boilerplate .htaccess File to Drupal Root, 
					check if "bp_d7.htaccess" is existing in the bp960 misc folder.'), 'error');
				}
			}
		}
	}
	// revert to drupal 7 .htaccess
	else if($form_state['values']['bp960_overwrite_htaccess'] == 'd7' && $htaccess_current == 'bp'){
		if(is_writable($htaccess))
		{
			if(copy($htaccess_d7, $htaccess))
			{
				if(unlink($htaccess_d7))
				{
					drupal_set_message(t('.htaccess successfully reverted to Drupal 7 defaults.'));	
				}
				else{
					drupal_set_message(t('Error while copy Drupal 7 Backup .htaccess File to Drupal Root, 
					check if "d7.htaccess" is existing in the Drupal Root.'), 'error');
				}
			}	
		}
	}
}