<?php

/**
 * The default group for NineSixty framework CSS files added to the page.
 */
define('CSS_NS_FRAMEWORK', -200);

function bp960_preprocess_html(&$vars, $hook){
    // IE Conditional Comment classes for html tag -- boilerplate
    $html_class  = 'no-js';
    $html_class .=  stristr($_SERVER['HTTP_USER_AGENT'], 'MSIE 6.') ? 'ie6':
                    stristr($_SERVER['HTTP_USER_AGENT'], 'MSIE 7.') ? 'ie7':
                    stristr($_SERVER['HTTP_USER_AGENT'], 'MSIE 8.') ? 'ie8':null;
    $vars['html_classes'] = $html_class;

    // add DD_belatedPNG.fix aka pngfix to footer for <= ie6
    if(stristr($_SERVER['HTTP_USER_AGENT'], 'MSIE 6.')){
        drupal_add_js(path_to_theme() . '/js/libs/dd_belatedpng.js', array('scope' => 'footer', 'weight' => 0));
        drupal_add_js('DD_belatedPNG.fix("img, .png_bg");', array('type' => 'inline', 'scope' => 'footer', 'weight' => 1));
    }
    
    // add Google Analytics tracking, as long as activated in the settings
    $ga_id = theme_get_setting('bp960_google_analytics_id'); // Google Analytics id
    if(theme_get_setting('bp960_google_analytics_status') && !empty($ga_id)){
   	 	drupal_add_js(
   	 	'var _gaq=[["_setAccount","'.$ga_id.'"],["_trackPageview"]];
   	 	(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];g.async=1;
   	 	g.src=("https:"==location.protocol?"//ssl":"//www")+".google-analytics.com/ga.js";
   	 	s.parentNode.insertBefore(g,s)}(document,"script"));', 
   	 	array('type' => 'inline', 'scope' => 'footer', 'weight' => 2));
    }
    
	if(theme_get_setting('bp960_debug_grid'))
	{
    	$vars['classes_array'][] = 'show-grid';
    }
}

function bp960_page_alter($page){
    // add viewport meta tag to head -- boilerplate
    $meta_viewport = array(
            '#type' => 'html_tag',
            '#tag' => 'meta',
            '#attributes' => array(
                'name' => 'viewport',
                'content' =>  'width=device-width, initial-scale=1.0'
            )
    );
    drupal_add_html_head($meta_viewport, 'meta_viewport');

    // add apple touch icon -- boilerplate
    $link_apple = array(
        '#tag' => 'link',
        '#attributes' => array(
            'href' => url(null, array('absolute' => true)) . drupal_get_path('theme', 'bp960') . '/apple-touch-icon.png',
            'rel' => 'apple-touch-icon',
        ),
    );
    drupal_add_html_head($link_apple, 'apple_touch_icon');
}

function bp960_preprocess_page(&$vars, $hook) {

  // For easy printing of variables.
  $vars['logo_img'] = '';
  if (!empty($vars['logo'])) {
    $vars['logo_img'] = theme('image', array(
      'path'  => $vars['logo'],
      'alt'   => t('Home'),
      'title' => t('Home'),
    ));
  }
  $vars['linked_logo_img']  = '';
  if (!empty($vars['logo_img'])) {
    $vars['linked_logo_img'] = l($vars['logo_img'], '<front>', array(
      'attributes' => array(
        'rel'   => 'home',
        'title' => t('Home'),
      ),
      'html' => TRUE,
    ));
  }
  $vars['linked_site_name'] = '';
  if (!empty($vars['site_name'])) {
    $vars['linked_site_name'] = l($vars['site_name'], '<front>', array(
      'attributes' => array(
        'rel'   => 'home',
        'title' => t('Home'),
      ),
    ));
  }

  // Site navigation links.
  $vars['main_menu_links'] = '';
  if (isset($vars['main_menu'])) {
    $vars['main_menu_links'] = theme('links__system_main_menu', array(
      'links' => $vars['main_menu'],
      'attributes' => array(
        'id' => 'main-menu',
        'class' => array('inline', 'main-menu'),
      ),
      'heading' => array(
        'text' => t('Main menu'),
        'level' => 'h2',
        'class' => array('element-invisible'),
      ),
    ));
  }
  $vars['secondary_menu_links'] = '';
  if (isset($vars['secondary_menu'])) {
    $vars['secondary_menu_links'] = theme('links__system_secondary_menu', array(
      'links' => $vars['secondary_menu'],
      'attributes' => array(
        'id'    => 'secondary-menu',
        'class' => array('inline', 'secondary-menu'),
      ),
      'heading' => array(
        'text' => t('Secondary menu'),
        'level' => 'h2',
        'class' => array('element-invisible'),
      ),
    ));
  }

}


/**
 * Contextually adds 960 Grid System classes.
 *
 * The first parameter passed is the *default class*. All other parameters must
 * be set in pairs like so: "$variable, 3". The variable can be anything available
 * within a template file and the integer is the width set for the adjacent box
 * containing that variable.
 *
 *  class="<?php print ns('grid-16', $var_a, 6); ?>"
 *
 * If $var_a contains data, the next parameter (integer) will be subtracted from
 * the default class. See the README.txt file.
 */
function ns() {
  $args = func_get_args();
  $default = array_shift($args);
  // Get the type of class, i.e., 'grid', 'pull', 'push', etc.
  // Also get the default unit for the type to be procesed and returned.
  list($type, $return_unit) = explode('-', $default);

  // Process the conditions.
  $flip_states = array('var' => 'int', 'int' => 'var');
  $state = 'var';
  foreach ($args as $arg) {
    if ($state == 'var') {
      $var_state = !empty($arg);
    }
    elseif ($var_state) {
      $return_unit = $return_unit - $arg;
    }
    $state = $flip_states[$state];
  }

  $output = '';
  // Anything below a value of 1 is not needed.
  if ($return_unit > 0) {
    $output = $type . '-' . $return_unit;
  }
  return $output;
}

/**
 * Implements hook_css_alter.
 * 
 * This rearranges how the style sheets are included so the framework styles
 * are included first.
 *
 * Sub-themes can override the framework styles when it contains css files with
 * the same name as a framework style. This mirrors the behavior of the 6--1
 * release of NineSixty warts and all. Future versions will make this obsolete.
 */
function bp960_css_alter(&$css) {
  global $theme_info, $base_theme_info;

  // Dig into the framework .info data.
  $framework = !empty($base_theme_info) ? $base_theme_info[0]->info : $theme_info->info;

  // Ensure framework CSS is always first.
  $on_top = CSS_NS_FRAMEWORK;

  // Pull framework styles from the themes .info file and place them above all stylesheets.
  if (isset($framework['stylesheets'])) {
    foreach ($framework['stylesheets'] as $media => $styles_from_960) {
      foreach ($styles_from_960 as $style_from_960) {
        // Force framework styles to come first.
        if (strpos($style_from_960, 'framework') !== FALSE) {
          $css[$style_from_960]['group'] = $on_top;
          // Handle styles that may be overridden from sub-themes.
          foreach (array_keys($css) as $style_from_var) {
            if ($style_from_960 != $style_from_var && basename($style_from_960) == basename($style_from_var)) {
              $css[$style_from_var]['group'] = $on_top;
            }
          }
        }
      }
    }
  }
}